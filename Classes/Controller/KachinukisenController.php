<?php
namespace Exotherme\Kachinukisen\Controller;

use Exotherme\Kachinukisen\Domain\Repository\EventRepository;
use TYPO3\Flow\Annotations as Flow;

class KachinukisenController extends \TYPO3\Flow\Mvc\Controller\ActionController
{

    /**
     * @Flow\Inject
     * @var EventRepository
     */
    protected $eventRepository;

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->view->assign('events', $this->eventRepository->findAll());
    }

}
